﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using utb_eshop_patek.Application.Admin.ApplicationServices.Security;
using utb_eshop_patek.Application.Admin.ViewModels.Security;
using utb_eshop_patek.Areas.Admin.Controllers.Common;

namespace utb_eshop_patek.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class SecurityController : AdminController
    {
        private readonly ISecurityApplicationService _securityApplicationService;

        public SecurityController(ISecurityApplicationService securityApplicationService)
        {
            _securityApplicationService = securityApplicationService;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            await _securityApplicationService.Login(viewModel);
            return RedirectToAction("Index", "Products", new { area = "Admin" });
        }

        public async Task<IActionResult> Logout()
        {
            await _securityApplicationService.Logout();
            return RedirectToAction(nameof(Login));
        }
    }
}