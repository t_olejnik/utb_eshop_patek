﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace utb_eshop_patek.Domain.Services.Files
{
    public class FilesHandler : IFilesHandler
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public FilesHandler(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public string SaveImage(IFormFile file)
        {
            var relativePath = $"images/products/{Guid.NewGuid()}.{file.ContentType.Replace("image/", string.Empty)}";
            var absolutePath = Path.Combine(_hostingEnvironment.WebRootPath, relativePath);

            using (var fileStream = new FileStream(absolutePath, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }

            return $"/{relativePath}";
        }
    }
}
