﻿using utb_eshop_patek.Domain.Entities.Products;

namespace utb_eshop_patek.Domain.Repositories.Products
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
