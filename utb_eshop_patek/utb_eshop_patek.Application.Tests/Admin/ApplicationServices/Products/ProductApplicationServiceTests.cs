﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using utb_eshop_patek.Application.Admin.ApplicationServices.Products;
using utb_eshop_patek.Application.Admin.ViewModels.Products;
using utb_eshop_patek.Application.Configuration;
using utb_eshop_patek.Domain.Entities.Products;
using utb_eshop_patek.Infrastructure.Data;
using Xunit;

namespace utb_eshop_patek.Application.Tests.Admin.ApplicationServices.Products
{
    public class ProductApplicationServiceTests : IDisposable
    {
        private readonly IProductApplicationService _productApplicationService;
        private readonly IServiceScope _scope;
        private IServiceProvider _serviceProvider => _scope.ServiceProvider;

        public ProductApplicationServiceTests()
        {
            _scope = Setup().CreateScope();
            _productApplicationService = _serviceProvider.GetService<IProductApplicationService>();
        }

        [Fact]
        public void GetIndexViewModel()
        {
            //Arrange
            var dataContext = _serviceProvider.GetService<DataContext>();
            dataContext.Products.AddRange(
                new Product { ID = 1, Name = "Kolo", Price = 69M},
                new Product { ID = 2, Name = "Auto", Price = 5M}
                );
            dataContext.SaveChanges();

            //Act
            var indexViewModel = _productApplicationService.GetIndexViewModel();

            //Assert
            Assert.NotNull(indexViewModel);
            Assert.Equal(indexViewModel.Products.Count, dataContext.Products.Local.Count);

        }

        [Fact]
        public void Insert()
        {
            //Arrange
            var image = _serviceProvider.GetService<IFormFile>();
            var vm = new ProductViewModel
            {
                Image = image,
                Name = "pepa",
                ID = 1,
                Price = 1M,
                ImageURL = ""
            };

            //Act
            var product = _productApplicationService.Insert(vm);

            //Assert
            Assert.NotNull(product);
            Assert.False(product.ID == 0);
            Assert.Equal(product.Name, vm.Name);
            Assert.Equal(product.Price, vm.Price);
            Assert.Equal(product.ImageURL, vm.ImageURL);
        }

        private IServiceProvider Setup()
        {
            var services = new ServiceCollection();
            var enviroment = new HostingEnvironment()
            {
                WebRootPath = @"C:\Users\student\Documents\Olejnik_PWT\eshop\utb_eshop_patek\utb_eshop_patek\utb_eshop_patek.Application.Tests\Admin\images\output",
                ContentRootPath = @"C:\Users\student\Documents\Olejnik_PWT\eshop\utb_eshop_patek\utb_eshop_patek\utb_eshop_patek.Application.Tests\Admin\images\output"
            };

            var builder = new ContainerBuilder();
            builder.RegisterModule<ApplicationDependencyModule>();
            ServiceConfiguration.Load(services, enviroment);
            MockIFormFile(services);
            builder.Populate(services);

            return new AutofacServiceProvider(builder.Build());
        }

        private void MockIFormFile(ServiceCollection services)
        {
            var fileMock = new Mock<IFormFile>();
            var fileName = "input.jpg";
            var fileInfo = new FileInfo($@"C:\Users\student\Documents\Olejnik_PWT\eshop\utb_eshop_patek\utb_eshop_patek\utb_eshop_patek.Application.Tests\Admin\images\input\{fileName}");
            var data = new byte[fileInfo.Length];

            using (var fs = fileInfo.OpenRead())
            {
                fs.Read(data, 0, data.Length);
            }

            using (var ms = new MemoryStream())
            {
                using (var writer = new StreamWriter(ms))
                {
                    writer.Write(data);
                    writer.Flush();
                    ms.Position = 0;
                    fileMock.Setup(x => x.OpenReadStream()).Returns(ms);
                    fileMock.Setup(x => x.FileName).Returns(fileName);
                    fileMock.Setup(x => x.Length).Returns(ms.Length);
                    fileMock.Setup(x => x.ContentType).Returns("image/jpg");
                }
            }

            services.AddSingleton(fileMock.Object);
        }

        public void Dispose() => _scope.Dispose();
    }
}
