﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Admin.ViewModels.Products
{
    public class IndexViewModel
    {
        public List<ProductViewModel> Products { get; set; }
    }
}
