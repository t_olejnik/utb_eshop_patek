﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using utb_eshop_patek.Application.Admin.ViewModels.Security;

namespace utb_eshop_patek.Application.Admin.ApplicationServices.Security
{
    public interface ISecurityApplicationService
    {
        Task<bool> Login(LoginViewModel viewModel);
        Task Logout();
        Task RegisterAndLogin(LoginViewModel viewModel);
    }
}
